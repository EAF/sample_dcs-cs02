﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//using Microsoft.Win32;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using System.IO;
using System.Collections;

namespace Sample_DCS_CS02
{
    //create delegate that allows other threads to send the Form
    delegate void delegateString(char T, string str);
    delegate void delegateString2(string str);

    public partial class Form1 : Form
    {
        //
        Socket clientSocket;

        //reading thread
        Thread DataReceived;

        //XML Message ID
        int messageID;

        // XSD informations
        String InterfaceLevel;
        String XSDLocalPath;
        String XSDVersion;

        //constructor
        public Form1()
        {
            InitializeComponent();
        }

        //load application variables
        private void Form1_Load(object sender, EventArgs e)
        {
            //resize ListViewEvents Columns
            listViewEvents.Columns[0].Width = 150;
            listViewEvents.Columns[1].Width = 500;

            //select new tabPage
            //tabControlLessons.SelectedTab = tabControlLessons.TabPages[1];

            //load Environment Variables
            loadEnvironmentVariables();

            //load text goals
            loadGoals();
        }

        //load Environment Variables
        private void loadEnvironmentVariables()
        {
            textBoxHost.Text = System.Environment.GetEnvironmentVariable("CUPPSPN", EnvironmentVariableTarget.User);
            textBoxPort.Text = System.Environment.GetEnvironmentVariable("CUPPSPP", EnvironmentVariableTarget.User);

            textBoxPLT.Text = System.Environment.GetEnvironmentVariable("CUPPSPLT", EnvironmentVariableTarget.User);
            textBoxCN.Text = System.Environment.GetEnvironmentVariable("CUPPSCN", EnvironmentVariableTarget.User);
            textBoxACN.Text = System.Environment.GetEnvironmentVariable("CUPPSACN", EnvironmentVariableTarget.User);

            textBoxOPR.Text = System.Environment.GetEnvironmentVariable("CUPPSOPR", EnvironmentVariableTarget.User);
            textBoxUN.Text = System.Environment.GetEnvironmentVariable("CUPPSUN", EnvironmentVariableTarget.User);

            textBoxXSDU.Text = System.Environment.GetEnvironmentVariable("CUPPSXSDU", EnvironmentVariableTarget.User);

        }

        //load text goals
        private void loadGoals()
        {
            richTextBoxGoals.AppendText("1. Send XML Message\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("2. Wait for response and manage errors\n");
            richTextBoxGoals.AppendText("   invalid XML message\n");
            richTextBoxGoals.AppendText("   invalid CUPPT message\n");
            richTextBoxGoals.AppendText("\n");
            richTextBoxGoals.AppendText("3.  \n");
        }

        /*
         Sockets
         */

        //connection-disconnection
        private void buttonConnect_Click(object sender, EventArgs e)
        {

            int port = int.Parse(textBoxPort.Text);
            string host = textBoxHost.Text;

            IPAddress[] addresses = Dns.GetHostAddresses(host);

            //if client does not exist
            if (clientSocket == null)
            {
                clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                DataReceived = new Thread(new ThreadStart(ReadMsg));

                messageID = 0;

            }


            //try to connect/disconnect
            try
            {
                if (!clientSocket.Connected)
                {
                    connect(host, port);
                    buttonXML.Enabled = true;

                }
                else
                {
                    //disconnect(host);
                    MessageBox.Show("Please send byeRequest to disconnect from the platform", "Airline Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (SocketException ex)
            {
                listViewEventsAdd(ex.Message);
            }
        }

        private void connect(String host, int port)
        {
            clientSocket.Connect(host, port);

            listViewEventsAdd("Connected to " + host);

            buttonConnect.Text = "1 - Network Socket Disconnection";

            //launch Thread and wait for message from server
            DataReceived.Start();
        }


        private void disconnect(String host)
        {
            listViewEventsAdd("Disconnected to " + host);
            buttonConnect.Text = "1 - Network Socket Connection";

            clientSocket.Close();
            DataReceived.Abort();

            clientSocket = null;
            DataReceived = null;
        }

        //load message
        private void buttonXML_Click(object sender, EventArgs e)
        {
            if (clientSocket != null)
            {

                //send message
                sendXML("interfaceLevelsAvailableRequest");

            }
        }

        private void sendXML(string message) {

            generateXML(message);

            sendMsg(richTextBoxXML.Text);

        }

        //write
        private void sendMsg(string XML)
        {

            messageID++;

            // replace XML vars
            XML = XML.Replace("{MESSAGEID}", messageID.ToString());
            XML = XML.Replace("{XSDVERSION}", XSDVersion);
            XML = XML.Replace("{INTERFACELEVEL}", InterfaceLevel);

            // send header msg
            XML = generateMsgHeader(XML) + XML;

            //encode string
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(XML);

            int DtSent = clientSocket.Send(msg, msg.Length, SocketFlags.None);

            if (DtSent == 0)
            {
                richTextBoxLogAppendText('E', "No message sent");
            }
            else
            {
                listViewEventsAdd("Message sent : " + XML);
                richTextBoxLogAppendText('S', XML);
            }

        }

        private String generateMsgHeader(String XML)
        {
            // send header msg
            String version = "01";
            String convention = "00";
            String size = XML.Length.ToString("X").ToUpper();


            while (size.Length < 6)
                size = "0" + size;

            Console.WriteLine(XML.Length);
            Console.WriteLine(size);
            
            return version + convention + size;
        }

        //read
        private void ReadMsg()
        {
            try
            {
                while (true)
                {
                    if (clientSocket.Connected)
                    {
                        //if client get data
                        if (clientSocket.Available > 0)
                        {

                            string messageReceived = null;
                            string rtfContent = null;

                            while (clientSocket.Available > 0)
                            {

                                try
                                {

                                    byte[] msg = new Byte[clientSocket.Available];

                                    //receive them

                                    clientSocket.Receive(msg, 0, clientSocket.Available, SocketFlags.None);

                                    messageReceived = System.Text.Encoding.UTF8.GetString(msg).Trim();

                                    //It concatenates the received data (4k max) in a variable

                                    rtfContent += messageReceived;

                                }

                                catch (SocketException E)
                                {
                                    this.Invoke(new delegateString2(listViewEventsAdd), "CheckData read" + E.Message);

                                }
                            }

                            try
                            {
                                //Filling the richtextbox with the data received 
                                // when it was all approved


                                this.Invoke(new delegateString2(listViewEventsAdd), "Message received : " + rtfContent);

                                this.Invoke(new delegateString(richTextBoxLogAppendText), 'R', rtfContent);

                                this.Invoke(new delegateString2(XMLReader), rtfContent);
                            }

                            catch (Exception E)
                            {
                                MessageBox.Show(E.Message);
                            }

                        }

                    }

                    //We wait for 10 milliseconds, to avoid the microprocessor runaway

                    Thread.Sleep(10);
                }

            }

            catch
            {
                //This thread is likely to be arrested at any time 
                //catch the exception so as not to display a message to the user

                Thread.ResetAbort();
            }

        }

        /*
          XML
         */

        private void XMLReader(String messageXML)
        {
            StringBuilder output = new StringBuilder();

            richTextBoxXMLValidate.Clear();


            String xmlString = messageXML.Substring(messageXML.IndexOf("<"));

            Console.WriteLine("Prefix : " + messageXML.Substring(0, messageXML.IndexOf("<")));

            // Create an XmlReader
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
                {
                    while (reader.Read())
                    {
                        // Only detect start elements.
                        if (reader.NodeType != XmlNodeType.EndElement)
                        {
                            richTextBoxXMLValidate.AppendText(reader.Name);
                            if (reader.NodeType == XmlNodeType.Text)
                            {
                                richTextBoxXMLValidate.AppendText("Node Value : \"" + reader.Value + "\"");
                            }

                            richTextBoxXMLValidate.AppendText("\n");

                            // display attributes http://msdn.microsoft.com/en-us/library/by2bd43b(v=vs.80).aspx
                            if (reader.HasAttributes)
                            {
                                while (reader.MoveToNextAttribute())
                                {
                                    richTextBoxXMLValidate.AppendText("\t" + reader.Name + " : \"" + reader.Value + "\"\n");
                                }
                                // Move the reader back to the element node.
                                reader.MoveToElement();
                            }
                        }
                    }

                    XmlDocument document = new XmlDocument();

                    document.LoadXml(xmlString);


                    XmlNode node = document.GetElementsByTagName("cupps").Item(0);

                    String messageName = node.Attributes.GetNamedItem("messageName").Value;

                    if (messageName.Equals("interfaceLevelsAvailableResponse"))
                    {

                        //change tabPage
                        tabControlLessons.SelectedTab = tabControlLessons.TabPages[1];

                        XmlNodeList nodeList = document.GetElementsByTagName("interfaceLevel");
                        for (int i = 0; i < nodeList.Count; i++)
                        {
                            node = nodeList[i];

                            if (node.Attributes.Count > 0)
                            {
                                XmlAttributeCollection attrs = node.Attributes;

                                ListViewItem lvItem = new ListViewItem(attrs.GetNamedItem("level").Value);

                                lvItem.SubItems.Add(attrs.GetNamedItem("wsLocalPath").Value);
                                lvItem.SubItems.Add(attrs.GetNamedItem("xsdVersion").Value);

                                listViewInterfaceLevels.Items.Add(lvItem);

                            }
                        }
                    }

                }
            }
            catch (Exception E)
            {
                //output.AppendLine(E.GetType().ToString);
                //output.AppendLine(E.Message);
                richTextBoxXMLValidate.Text = E.Message;
            }

            Console.WriteLine(output.ToString());
            //OutputTextBlock.Text = output.ToString();
        }

        /*
         
        */


        //function assigned to the delegate
        private void richTextBoxLogAppendText(char T, string str)
        {
            string symbol = (T == 'S') ? "-->" : "<--";


            richTextBoxLog.AppendText(T + " " + symbol + " " + DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss.fff") + " :\n" + str + "\n\r");
            richTextBoxLog.ScrollToCaret();
        }

        //function assigned to the delegate
        private void richTextBoxXMLRequestAppendText(string str)
        {
            //richTextBoxXMLRequest.Text = str;
        }


        //add Events in ListView
        private void listViewEventsAdd(string str)
        {
            ListViewItem lvItem = new ListViewItem(DateTime.Now.ToString("dd/mm/yyyy HH:mm:ss.fff"));
            lvItem.SubItems.Add(str);

            listViewEvents.Items.Add(lvItem);
        }

        //increase messageID in XML Message
        private void generateXML(String type)
        {
            //header
            String XML = "<cupps xmlns=\"http://www.cupps.aero/cupps/01.03\" messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName = \""
                    + type + "\">";

            if (type == "interfaceLevelsAvailableRequest")
            {
                XML += "<interfaceLevelsAvailableRequest hsXSDVersion=\"{XSDVersion}\"/>";
            }
            else if (type == "interfaceLevelRequest")
            {
                XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
            }
            else if (type == "authenticateRequest")
            {
                XML += "<authenticateRequest airline=\"{AIRLINE}\" eventToken=\"{APPLICATIONCODE}\" platformDefinedParameter=\"{PLATFORMDEFINEDPARAMETER}\" >";

                XML += "<applicationList>";
                XML += "<application applicationName=\"{APPLICATIONNAME}\" applicationVersion=\"{APPLICATIONVERSION}\" applicationData=\"{APPLICATIONDATA}\"/>";
                XML += "</applicationList>";

                XML += "</authenticateRequest>";
            }
            else if (type == "deviceQueryRequest")
            {
                XML += "<deviceQueryRequest deviceName=\"{DEVICENAME}\"/>";
            }
            else if (type == "deviceAcquireRequest")
            {
                XML += "<deviceAcquireRequest deviceName=\"{DEVICENAME}\" deviceToken=\"{DEVICETOKEN}\" airlineID=\"{AIRLINE}\"/>";

                /*if (!textHostName.getText().equals("") && !textHostIP.getText().equals("")
                        && !textHostPort.getText().equals(""))
                    XML += "<relayTo hostName=\"{HOSTNAME}\" ip=\"{HOSTIP}\" port=\"{HOSTPORT}\"/>";*/
            }
            else if (type == "interfaceModeRequest")
            {

                XML += "<interfaceModeRequest mode=\"{INTERFACEMODE}\"></interfaceModeRequest>";

            }
            else if (type == "deviceLockRequest")
            {

                XML += "<deviceLockRequest renew=\"true\"></deviceLockRequest>";

            }
            else if (type == "deviceUnlockRequest")
            {

                XML += "<deviceUnlockRequest></deviceUnlockRequest>";

            }
            else if (type == "aeaRequest")
            {

                XML += "<aeaRequest>";
                XML += "<aeaText><![CDATA[{AEATEXT}]]></aeaText>";

                /*if (AeaRequestType.equals("pcx"))
                {
                    XML += "<aeaBinary><![CDATA[{AEABINARY}]]></aeaBinary>";
                }*/

                XML += "</aeaRequest>";

            }
            else if (type == "deviceReleaseRequest")
            {

                XML += "<deviceReleaseRequest></deviceReleaseRequest>";

            }
            else if (type == "readerReadRequest")
            {

                XML += "<readerReadRequest></readerReadRequest>";

            }

            // end
            XML += "</cupps>";

            //richTextBoxXML.Text = XML;
            richTextBoxXML.Text = XML;
        }

        /*
         other events
         */

        //clear messages whend doubleclick
        private void richTextBoxLog_DoubleClick(object sender, EventArgs e)
        {
            //richTextBoxLog.Clear();
        }

        //before closing, checks whether the socket is not connected
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (clientSocket != null && clientSocket.Connected)
            {
                //MessageBox.Show("You are connected !", "Airline Application");
                //e.Cancel = true;
                disconnect(textBoxHost.Text);
            }
            else
            {
                /*
                // Display a MsgBox asking the user to save changes or abort.
                if (MessageBox.Show("Are you sure you want to quit ?", "Airline Application", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    // Cancel the Closing event from closing the form.
                    e.Cancel = true;

                }
                */
            }
        }

        private void buttonInterfaceLevelRequest_Click(object sender, EventArgs e)
        {
            if (listViewInterfaceLevels.SelectedItems.Count > 0)
            {
                InterfaceLevel = listViewInterfaceLevels.SelectedItems[0].SubItems[0].Text;
                XSDLocalPath = listViewInterfaceLevels.SelectedItems[0].SubItems[1].Text;
                XSDVersion = listViewInterfaceLevels.SelectedItems[0].SubItems[2].Text;

                //send message
                sendXML("interfaceLevelRequest");

            }
            else
            {
                MessageBox.Show("Please select Interface Level", "Airline Application", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }

        private void buttonUnsupportedInterfaceLevelRequest_Click(object sender, EventArgs e)
        {
            InterfaceLevel = "01.02";
            XSDLocalPath = "01.02";
            XSDVersion = "01.02";

            // send message		
            sendXML("interfaceLevelRequest");
        }

        private void buttonObsoleteInterfaceLeveRequest_Click(object sender, EventArgs e)
        {
            InterfaceLevel = "00.00";
            XSDLocalPath = "00.00";
            XSDVersion = "00.00";

            //send message
            sendXML("interfaceLevelRequest");
        }


    }
}
